function strikeRate(matches,deliveries){
    const totalRuns = {};
    const totalBalls = {};
    const results={};
    
    for (let match of matches) {

    const season = match.season;
    
    for(let delivery of deliveries){

      const batsman = delivery.batsman;
      const runs = delivery.batsman_runs;
      
    
      if (batsman=='V Kohli') {
      
        if(totalRuns[season]&&totalBalls[season]){
  
            totalRuns[season]+=(runs*1);
            totalBalls[season]+=1;

          }else{

            totalRuns[season]=(runs*1);
            totalBalls[season]=1;

          }
        }
    }
  }

  for(let year in totalRuns){
    results[year]={}
    for(let ball in totalRuns){
      results[year]=(totalRuns[year]/totalBalls[ball])*100;
    }
  }

console.log(results)
return results;

}
module.exports = strikeRate;






