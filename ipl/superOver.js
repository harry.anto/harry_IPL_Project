function superOver(deliveries){

const result={};
const balls={};
const runs={};

for(let delivery of deliveries){

    let superover = delivery.is_super_over;
    let bowler= delivery.bowler;
    let run = delivery.total_runs;

    if(superover=='1'){

        if(runs[bowler]&&balls[bowler]){
            
            runs[bowler]+=(run*1);
            balls[bowler]+=(superover*1);

        }else {
            
            runs[bowler]=(run*1);
            balls[bowler]=(superover*1);

        }
    }
}

for (let bowlers in runs) {

    for (let ball in balls) {

        result[bowlers] = (runs[bowlers] / balls[ball]);

    }
}


arr1=Object.entries(result)
arr2=[...arr1.sort((a,b)=>a[1]-b[1])]
arr2=[...arr2.slice(0,1)]
arr2=[...arr2.flat(1)]

let obj={}
for(let i=0;i<=1;i++){
  obj[arr2[0]]=arr2[1]
}

return obj;

}

module.exports = superOver;