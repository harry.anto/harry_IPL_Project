function mostPlayerAwards(matches){
  const result = {};
  const newObj={}
  let arr1=[];
  let arr2=[];
  let arr3=[];
  let arr4=[];
  for (let match of matches) {
    var season = match.season;
    const player = match.player_of_match;
    if(result[season]){
      if(result[season][player]){
          result[season][player]+=1;
      }else{
          result[season][player]=1;
          arr4.push(season);
      }
  }else{
      result[season]={};
  }      
}

let arr5=[...new Set(arr4)]
arr5.sort()

for(let season in result){
  if(result[season]){
    arr1=Object.entries(result[season])
    arr1.sort((a,b)=>b[1]-a[1]);
  }
  arr2.push(arr1.slice(0,1));
}
arr3=[...arr2.flat(2)]
let arr6=arr3.filter(x => isNaN(x));

for(let i=0;i<arr5.length;i++){
  newObj[arr5[i]]=arr6[i]
}
return newObj;
}

module.exports = mostPlayerAwards;



