function wonTossMatch(matches){
    const result = {};
    for (let match of matches) {
      const toss = match.toss_winner;
      const winner = match.winner;
      if(toss==winner){
        if(result[winner]){
          result[winner]+=1
        }else{
          result[winner]=1
        }
      }
    }
    return result;
}

module.exports = wonTossMatch;