const fs = require("fs");
const csv = require("csvtojson");
const wonTossMatch = require("../ipl/wonTossMatch")
const mostPlayerAwards = require("../ipl/mostPlayerAwards")
const strikeRate = require("../ipl/strikeRate");
const dismissal = require("../ipl/dismissal");
const superOver = require("../ipl/superOver");

const matchesFilePath='./data/matches.csv';
const deliveriesFilePath='./data/deliveries.csv';

const outputFilePath='./output/data.json';

function main(){
    csv()
    .fromFile(matchesFilePath)
    .then(matches =>{
        csv()
        .fromFile(deliveriesFilePath)
        .then(deliveries =>{

        let result_final={}
        
        let result1 = wonTossMatch(matches);
        let result2 = mostPlayerAwards(matches);
        let result3 = strikeRate(matches,deliveries);
        let result4 = dismissal(deliveries);
        let result5 = superOver(deliveries);
            
        result_final['Won toss and match'] = result1;
        result_final['Most Player Awards'] = result2;
        result_final['strike rate of V Kholi'] = result3;
        result_final['dismissed MS Dhoni'] = result4;
        result_final['super over economy'] = result5;

        const jsonString = JSON.stringify(result_final);
        fs.writeFile(outputFilePath, jsonString, 'utf-8',err => {
            if(err){
                console.log(err)
            }
        }); 
    });
});
}


main()
